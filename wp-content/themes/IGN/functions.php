<?php

/*if ( current_user_can('delete_others_pages') ) {
    add_action('wp_footer', 'show_template');
    function show_template() {
        global $template;
        print_r($template);
        edit_post_link();
    }
}*/


include STYLESHEETPATH . '/core/includes/custom-plugins.php';

function change_default_title( $title ) {
  $screen = get_current_screen();

  if  ( 'people' == $screen->post_type ) {
    $title = 'Enter Persons Name';
  }

  return $title;
}


function my_custom_mime_types( $mimes ) {
 
// New allowed mime types.
$mimes['svg'] = 'image/svg+xml';
$mimes['svgz'] = 'image/svg+xml';
$mimes['doc'] = 'application/msword';
 
// Optional. Remove a mime type.
unset( $mimes['exe'] );
 
return $mimes;
}
add_filter( 'upload_mimes', 'my_custom_mime_types' );


add_filter( 'enter_title_here', 'change_default_title' );
add_theme_support( 'category-thumbnails' );
add_filter('deprecated_constructor_trigger_error', '__return_false');  


 
 


//Add Image Sizes To Post / Page Admin
add_filter( 'image_size_names_choose', 'my_image_sizes' );
function my_image_sizes( $sizes ) {
  $addsizes = array(
    "content-image" => __( "Content Image" ),
  );
  $newsizes = array_merge( $sizes, $addsizes );
  return $newsizes;
}


// Custom Functions
function get_excerpt( $limit, $source = null ) {
  global $post;
  if ( $source == "content" ? ( $excerpt = get_the_content() ) : ( $excerpt = get_the_excerpt() ) );
  $excerpt = preg_replace( " (\[.*?\])", '', $excerpt );
  $excerpt = strip_shortcodes( $excerpt );
  $excerpt = strip_tags( $excerpt );
  $excerpt = substr( $excerpt, 0, $limit );
  $excerpt = substr( $excerpt, 0, strripos( $excerpt, " " ) );
  $excerpt = trim( preg_replace( '/\s+/', ' ', $excerpt ) );
  $excerpt = $excerpt.'... <a class="read-more" href="'.get_permalink( $post->ID ).'">[more]</a>';
  return $excerpt;
}

function ShortenText( $limit, $text, $word ) { // limit, text, keep whole word
  $chars_limit = $limit; // Character length
  $chars_text = strlen( $text );
  $text = $text." ";
  $text = substr( $text, 0, $chars_limit );
  if ( $word == true ) {
    $text = substr( $text, 0, strrpos( $text, ' ' ) );
  }

  if ( $chars_text > $chars_limit ) { $text = $text."..."; } // Ellipsis
  return $text;
}


// function my_acf_options_page_settings( $settings ) {
//   $settings['title'] = 'Options';
//   if ( current_user_can( 'add_users' ) ) {
//     $settings['pages'] = array( 'Major Partners', 'Community Sponsors', 'Home Page Buttons', 'Social Count', 'Home Advertising', 'Home Splash', 'Image Defaults', 'Admin', 'Unused AFC' );
//   }else {
//     $settings['pages'] = array( 'Major Partners', 'Community Sponsors', 'Home Advertising', 'Home Splash', 'Image Defaults', );
//   }


//   return $settings;
// }
// add_filter( 'acf/options_page/settings', 'my_acf_options_page_settings' );


// Social Media Links
function fsp_adv_social_media() {
  $output = '
        <ul class="fsp-adv-social-media">
        <li class="share-title">Share</li>
        <li><a href="https://www.facebook.com/sharer.php?u='.get_the_permalink().'&t='.get_the_title().'" target="_blank">f</a></li>
        <li><a href="https://twitter.com/home?status='.get_the_title().'%20-%20'.get_the_permalink().'" target="_blank">t</a></li>
        <li><a href="https://plus.google.com/share?url='.get_the_permalink().'" target="_blank">m</a></li>
        <li><a href="https://plus.google.com/share?url='.get_the_permalink().'" target="_blank">P</a></li>
        </ul>
  ';
  IF (function_exists('sharing_display')){
        $output = sharing_display();
  }
  return $output;
}
// Use the after_setup_theme hook with a priority of 11 to load after the
// parent theme, which will fire on the default priority of 10
add_action( 'after_setup_theme', 'remove_featured_images_from_pages', 11 );

function remove_featured_images_from_pages() {

  // This will remove support for post thumbnails on ALL Post Types
  remove_theme_support( 'post-thumbnails' );

  // Add this line in to re-enable support for just Posts
  add_theme_support( 'post-thumbnails', array( 'post','gallery') );
}

// Register Child Scripts
function child_scripts() {
/*
   wp_register_script( 'flexisel', get_stylesheet_directory_uri() . '/core/js/jquery.flexisel.js', array( 'jquery' ), '1', false );
   wp_enqueue_script( 'flexisel' );
   wp_register_script( 'slick', get_stylesheet_directory_uri() . '/core/js/slick.min.js', array( 'jquery' ), '1', true );
  wp_enqueue_script( 'slick' ); 
   wp_register_script( 'classie', get_stylesheet_directory_uri() . '/core/js/classie.js', array( 'jquery' ), '1', true );
  wp_enqueue_script( 'classie' );
  wp_register_script( 'uisearch', get_stylesheet_directory_uri() . '/core/js/uisearch.js', array( 'jquery' ), '1', true );
  wp_enqueue_script( 'uisearch' );  */ 
   wp_register_script( 'jspdf', get_stylesheet_directory_uri() . '/core/js/jspdf.min.js', array( 'jquery' ), '1', true );
  wp_enqueue_script( 'jspdf' ); 
 
}
add_action( 'wp_enqueue_scripts', 'child_scripts' );

 


 
 

if( function_exists('acf_add_options_page') ) {
  

acf_add_options_page(array(
    'page_title'  => 'Site Options',
    'menu_title'  => 'Site Options',
    'menu_slug'   => 'options',
    'capability'  => 'edit_posts',
    'redirect'    => false,
   
  ));
 

}

 
 
 
