<?php

// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * Pages Template
 */

get_header(); ?>
 
 
<div class="content-body-inner">
	<div class="container">
		
			<div id="content" class="col-1">
				<div class="col-md-12 col-xs-12 main-content">
				 
				 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<div class="page-title"><?php the_title(); ?></div>
					<div class="page-content"><?php the_content(); ?></div>
				<?php endwhile; else : ?>
					<h1>Post Not Found</h1>
				<?php endif; ?>

			<ul class="tabs">
				<?php $tabs = get_field('steps');
				$i = 0;
				foreach($tabs as $tab) { 
					$i++; 
					if($i == 1) { ?>

						<li class="tab-link current" data-tab="tab-<?php echo $i;?>"><?php echo $tab['tab'];?></li>

					<?php } else { ?>
					
						<li class="tab-link " data-tab="tab-<?php echo $i;?>"><?php echo $tab['tab'];?></li>
					<?php } 
				 
				 }?>
			</ul>
			<?php
			 
			if(have_rows('steps')) { //if starts	?>
				
				<div id="to-do-lists">
					<?php
					$j=0;
					// loop through rows (parent repeater)
					while( have_rows('steps') ): the_row(); 
						$j++;

						if($j == 1) { ?>

							<div id="tab-<?php echo $j;?>" class="tab-content current">

							<?php 
							$k = 0; 
								while( have_rows('content') ): the_row(); $k++;?>
									
									<div class="col-md-12 border col-xs-12">
										
										<div class="col-md-6 col-lg-3 col-xs-12 one">
											<div class="step"> Step <div> <?php echo $k; ?></div></div>
											<div class="name"><?php the_sub_field('name'); ?> </div>
										</div>

										<div class="col-md-6 col-lg-4 col-xs-12 two"> <?php  the_sub_field('steps1'); ?>  </div>
										<div class="col-md-12 col-lg-5 col-xs-12 three"> <?php  the_sub_field('step2'); ?> </div>

									</div>

								<?php endwhile; ?>

							<div class="start-browsing col-sm-12 col-xs-12">
					 			<img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/core/images/tick.svg">
					 			<span>That's it!</span>
					 			<a href="https://www.ignitioncasino.eu/">Start browsing now </a>
					 			<?php if(have_rows('extra')) :?>
						 		<div class="extra">
						 			
						 			<?php 
						 			
						 			while( have_rows('extra') ): the_row();?>

						 				<div class="col-lg-12 extra-inner">
						 					<div class="col-lg-7 col-sm-12 col-md-7 col-xs-12 one two">   
						 						<div class="image"><img class="icon" src="<?php the_sub_field('icon'); ?>" /></div>
						 						<div class="text">  <?php the_sub_field('text'); ?>   </div>
						 					</div>
						 					 
						 					<div class="col-lg-5 col-sm-12 col-md-5 col-xs-12 two three">  <?php the_sub_field('images'); ?> </div>
						 				</div>

						 			<?php endwhile;  ?>
						 		</div> 
						 	<?php endif;  ?>

				 		</div>
							</div>
						<?php  } else { ?>

						<div id="tab-<?php echo $j;?>" class="tab-content ">

							<?php 
							$k = 0; 
								while( have_rows('content') ): the_row(); $k++;?>
									
								<div class="col-md-12 border col-xs-12">
										
										<div class="col-md-6 col-lg-3 col-xs-12 one">
											<div class="step"> Step <div> <?php echo $k; ?></div></div>
											<div class="name"><?php the_sub_field('name'); ?> </div>
										</div>

										<div class="col-md-6 col-lg-4 col-xs-12 two"> <?php  the_sub_field('steps1'); ?>  </div>
										<div class="col-md-12 col-lg-5 col-xs-12 three"> <?php  the_sub_field('step2'); ?> </div>

									</div>
									<?php

								endwhile; ?>

								<div class="start-browsing col-xs-12 col-sm-12">
						 			<img class="icon" src="<?php bloginfo('stylesheet_directory'); ?>/core/images/tick.svg">
						 			<span>That's it!</span>
						 			<a href="https://www.ignitioncasino.eu/">Start browsing now </a>
								 	<?php if(have_rows('extra')): ?>
								 		<div class="extra">
								 			
								 			<?php 
								 				
								 			while( have_rows('extra') ): the_row();?>

								 			<div class="col-lg-12 extra-inner">
						 					<div class="col-lg-7 col-sm-12 col-md-7 col-xs-12 one two">   
						 						<div class="image"><img class="icon" src="<?php the_sub_field('icon'); ?>" /></div>
						 						<div class="text">  <?php the_sub_field('text'); ?>   </div>
						 					</div>
						 					 
						 					<div class="col-lg-5 col-sm-12 col-md-5 col-xs-12 two three">  <?php the_sub_field('images'); ?> </div>
						 				</div>
								 			<?php endwhile; ?>
								 		</div> 
					 				<?php endif; ?>


				 					</div>
							</div>

						<?php	}
						  ?>  
						
					<?php endwhile; // while( has_sub_field('to-do_lists') ):  ?>

					

				 		
					</div>
				<?php } //if ends ?>
				


 



	</div><!-- end col-1 -->
			 
	
	</div><!-- end of .container -->
</div><!-- end of .container -->


<?php get_footer(); ?>