<?php 
	$args = array(
		'post_type' => 'people',
		'posts_per_page' => '999',
		'ignore_sticky_posts' => 1,
	);
	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() ) {
?>
<div class="player-profile-container arrow-bt arrow-bt-white">
<h1> <?php echo get_field('player_title','option');?></h1>

	<ul class="player-profiles">
		<?php while ( $the_query->have_posts() ) {
		$the_query->the_post(); ?>
		<li>
			<a href="<?php the_permalink(); ?>">
				<span class="player-stats">
									<h4><?php the_title(); ?></h4>
									<div class="logo-number">
										<?php 
										$club_logo = get_field('club_logo');
										if( $club_logo ) {
											echo wp_get_attachment_image( $club_logo, 'full' );
										}else{ ?>
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/core/images/club-logo.png" alt="">
										<?php } ?>
										<?php if(get_field('player_number')){ ?><span class="player-number"><?php the_field('player_number'); ?></span><?php } ?>
									</div>
									<div class="player-stats-table">						
										<table>
											<?php if(get_field('height')){ ?><tr><td nowrap="nowrap" class="l-td"><b>HEIGHT:</b></td><td><?php the_field('height'); ?></td></tr><?php } ?>
											<?php if(get_field('weight')){ ?><tr><td nowrap="nowrap" class="l-td"><b>WEIGHT:</b></td><td><?php the_field('weight'); ?></td></tr><?php } ?>
											<?php if(get_field('jnr_club')){ ?><tr><td nowrap="nowrap" class="l-td"><b>JNR CLUB:</b></td><td><?php the_field('jnr_club'); ?></td></tr><?php } ?>
										</table>
									</div>
								</span>
								<?php 
								$player_image = get_field('player_image');
								if( $player_image ) {
									echo wp_get_attachment_image( $player_image, 'player-img' );
								}else{ ?>
									<img src="<?php echo get_stylesheet_directory_uri(); ?>/core/images/player.jpg" alt="">
								<?php } ?>
			</a>
		</li>
		<?php } wp_reset_postdata(); ?>
		
	</ul>

	<h3 class="block-btn"><a href="<?php echo get_field('full_players_list','option'); ?>" class="theme-btn">Full Player List</a></h3>
	
	<div class="clearfix"></div>

<script>
	jQuery(document).ready(function($) {
		$('.player-profiles').slick({
		  centerMode: true,
		  variableWidth: true,
		  // centerPadding: '50px',
		  slidesToShow: 3,
		  autoplay: true,
		  toplaySpeed: 5000,
		  infinite: true,
		  slide: 'li',
		  centerMode: true,
		  centerPadding: '0px',
		  responsive: [
		    {
		      breakpoint: 769,
		      settings: {
		        // arrows: false,
		        centerMode: true,
		        centerPadding: '0px',
		        slidesToShow: 3,
		        // variableWidth: false
		      }
		    },
		    {
		      breakpoint: 370,
		      settings: {
		        //arrows: false,
		        centerMode: true,
		        centerPadding: '0px',
		        slidesToShow: 1,
		        touchThreshold: 10
		      }
		    }
		  ]
		});

	});
</script>
<?php } ?>
</div>
<?php
	$btn_back = get_field('button_background','option');
 	$btn_title = get_field('button_title','option');
 	$theme_color = get_field('theme_color','option'); 
 	$info_text = get_field('player_info_text','option');
 	$arrow_back = get_field('arrow_background','option');
	$arrow = get_field('arrow','option');
 ?>


<style type="text/css">
	.player-profile-container .player-stats {
		background: <?php echo $theme_color ?>;
		color: <?php echo $info_text; ?>;
		
	}

	.player-profile-container .block-btn .theme-btn {
		background: <?php echo $btn_back;?>;
		color: <?php echo $btn_title; ?>;
	}
	.player-profile-container .slick-prev, .player-profile-container .slick-next {
		background: <?php echo $arrow_back;?> ;
	}
	.player-profile-container .slick-prev, .player-profile-container .slick-next {
		color:<?php echo $arrow;?>;
	}
</style>